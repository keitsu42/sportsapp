import { Injectable } from '@angular/core';
import {LeagueObjectStanding} from './LeagueTableObjects/standing';
import {Observable} from 'rxjs/Observable';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AppComponent} from './app.component';
import {League} from './LeagueTableObjects/league';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import {FixtureFixtures} from './Fixture/fixtures';

/*This adds the service to root, but only if it is used*/
const headers = new HttpHeaders().set('X-Auth-Token', 'd52950326bda4dd1a6b7ea675f7e3e30');

@Injectable()

export class StandingService {
  private static selectedStanding: LeagueObjectStanding;
  /*Uri to API*/
  private standingsUri = 'https://api.football-data.org/v1/competitions/' + AppComponent.leagueId + '/leagueTable';
  private fixturesUri = 'https://api.football-data.org/v1/competitions/' + AppComponent.leagueId + '/fixtures';
  cachedStandings: League;
  cachedFixtures: FixtureFixtures;
  static setSelectedStanding(standing: LeagueObjectStanding) {
    this.selectedStanding = standing;
    if (standing) {
      console.log('Selected team: ' + standing.teamName);
    } else {
      console.log('Selected team cleared');
    }
  }
  static getSelectedStanding() {
    return this.selectedStanding;
  }
  constructor(
    private http: HttpClient
  ) { }

  getStandings(): Observable<League> {
    /*IF data is already cached*/
    if (this.cachedStandings) {
      console.log('cache data for standings found');
      /*RETURN it is an observable*/
      return Observable.of(this.cachedStandings);
    } else  /*ELSE, get it from server and store in cache*/{
      console.log('cache data for standings NOT found');
      return this.http.get<League>(this.standingsUri, {headers})
        .do(data => this.cachedStandings = data);
    }
  }
  getFixtures(): Observable<FixtureFixtures> {
    /*IF data is already cached*/
    if (this.cachedFixtures) {
      console.log('cache data for fixtures found');
      /*RETURN it is an observable*/
      return Observable.of(this.cachedFixtures);
    } else  /*ELSE, get it from server and store in cache*/{
      console.log('cache data for fixtures NOT found');
      return this.http.get<FixtureFixtures>(this.fixturesUri, {headers})
        .do(data => this.cachedFixtures = data);
    }
  }

}
