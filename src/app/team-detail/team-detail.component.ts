import {Component, OnInit} from '@angular/core';
import { LeagueObjectStanding } from '../LeagueTableObjects/standing';
import {ActivatedRoute} from '@angular/router';
import {StandingService} from '../standing.service';
import {FixtureFixture} from '../Fixture/fixture';
import {HelperService} from '../helper.service';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})

export class TeamDetailComponent implements OnInit {
  standing: LeagueObjectStanding;
  standings: LeagueObjectStanding[];
  fixtures: FixtureFixture[];
  constructor(
    private route: ActivatedRoute,
    private standingService: StandingService,
  ) {}

  ngOnInit() {
    /*Update when route changes*/
    this.route.params.subscribe ( () => {
      this.validateForm();
    });
  }
  validateForm() {
    window.scrollTo(0, 0);
    this.getStanding();
    this.getFixtures();
  }
  getStanding(): void {
    this.standingService.getStandings()
      .subscribe(standingsObservable => this.standings = standingsObservable.standing, error => console.log('Error: ', error),
        () => this.getIndividualStanding());
  }
  getIndividualStanding(): void {
    this.standing = this.standings.find(standing => this.getTeamID(standing) === this.route.snapshot.paramMap.get('teamID'));
    this.setSelectedStanding(this.standing);
  }
  getFixtures(): void {
    this.standingService.getFixtures()
      .subscribe(fixturesObservable => this.fixtures = fixturesObservable.fixtures,
        error => console.log('Error: ', error), () => this.filterFixtures());
  }
  /*Filter the fixtures so that it is only for the relevant team*/
  filterFixtures(): void {
    this.fixtures = this.fixtures.filter(fixture => (fixture.awayTeamName === this.standing.teamName ||
      fixture.homeTeamName === this.standing.teamName) && fixture.status === 'FINISHED');
  }
  getTeamID(standing: LeagueObjectStanding) {
    return HelperService.getTeamID(standing);
  }
  getFixtureResult(fixture: FixtureFixture) {
    return HelperService.getFixtureResult(fixture, this.standing.teamName === fixture.homeTeamName);
  }
  setSelectedStanding(standing: LeagueObjectStanding) {
    StandingService.setSelectedStanding(standing);
  }
}
