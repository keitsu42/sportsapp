import {FixtureResult} from './result';

export class FixtureFixture {
  date: string;
  status: string;
  matchday: number;
  homeTeamName: string;
  awayTeamName: string;
  result: FixtureResult;
}
