import {FixtureFixture} from './fixture';

export class FixtureFixtures {
  count: number;
  fixtures: FixtureFixture[];
}
