import { Component } from '@angular/core';
import {LeagueObjectStanding} from './LeagueTableObjects/standing';
import {StandingService} from './standing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /*title = 'SportsApp';*/
  static leagueId = 446;
}
