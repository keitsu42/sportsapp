import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LeagueTableComponent } from './league-table/league-table.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import {StandingService} from './standing.service';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { TeamComparisonComponent } from './team-comparison/team-comparison.component';

@NgModule({
  declarations: [
    AppComponent,
    LeagueTableComponent,
    TeamDetailComponent,
    NavbarComponent,
    TeamComparisonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  /*Add providers which are used to retrieve information*/
  providers: [StandingService],
  bootstrap: [AppComponent]
})
export class AppModule {}
