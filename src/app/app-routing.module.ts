import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LeagueTableComponent} from './league-table/league-table.component';
import {TeamDetailComponent} from './team-detail/team-detail.component';
import {TeamComparisonComponent} from './team-comparison/team-comparison.component';

/*Define routes*/
/*
* path: a string that matches the URL in the browser address bar
* component: the component that the router should create when navigating to this route
* */
const routes: Routes = [
  /*Redirect to league table when nothing is selected*/
  { path: '', redirectTo: '/leaguetable', pathMatch: 'full'},
  /*Route for league table component*/
  { path: 'leaguetable', component: LeagueTableComponent},
  /*Route for selected team*/
  { path: 'team/:teamID', component: TeamDetailComponent},
  /*Route for team comparison*/
  { path: 'team-comparison', component: TeamComparisonComponent}
];

@NgModule({
  /*the RouterModule.forRoot() method supplies the service providers and directives
  * needed for routing, and performs the initial navigation based on the current browser url*/
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
