import { Component, OnInit } from '@angular/core';
import {LeagueObjectStanding} from '../LeagueTableObjects/standing';
import {StandingService} from '../standing.service';
import {HelperService} from '../helper.service';
import {FixtureFixture} from '../Fixture/fixture';

@Component({
  selector: 'app-team-comparison',
  templateUrl: './team-comparison.component.html',
  styleUrls: ['./team-comparison.component.css']
})
export class TeamComparisonComponent implements OnInit {
  /*Define variable to store standing array*/
  standings: LeagueObjectStanding[];
  teamAStanding: LeagueObjectStanding;
  teamBStanding: LeagueObjectStanding;
  fixtures: FixtureFixture[];
  constructor(
    private standingService: StandingService
  ) {}
  getStandings(): void {
    this.standingService.getStandings().subscribe(standingsObservable => this.standings
      = HelperService.sortStandings(standingsObservable.standing)); /*Get standings in alphabetical order*/
  }
  ngOnInit() {
    this.teamAStanding = StandingService.getSelectedStanding();
    StandingService.setSelectedStanding(null);
    this.getStandings();
    this.getFixtures();
  }
  setTeamAStanding(standing: LeagueObjectStanding) {
    if (standing === this.teamBStanding) {
      this.swapTeamStandings();
    } else {
      this.teamAStanding = standing;
    }
  }
  setTeamBStanding(standing: LeagueObjectStanding) {
    if (standing === this.teamAStanding) {
      this.swapTeamStandings();
    } else {
      this.teamBStanding = standing;
    }
  }
  swapTeamStandings() {
    const temp = this.teamAStanding;
    this.teamAStanding = this.teamBStanding;
    this.teamBStanding = temp;
  }
  getFixtures(): void {
    this.standingService.getFixtures()
      .subscribe(fixturesObservable => this.fixtures = fixturesObservable.fixtures);
  }
  getFilteredFixtures() {
    return this.fixtures.filter(fixture => (fixture.homeTeamName === this.teamAStanding.teamName && fixture.awayTeamName ===
      this.teamBStanding.teamName) || (fixture.homeTeamName === this.teamBStanding.teamName &&
      fixture.awayTeamName === this.teamAStanding.teamName) && fixture.status === 'FINISHED');
  }
  getFixtureWinner(fixture: FixtureFixture) {
    /*If home team won*/
    if (fixture.result.goalsHomeTeam > fixture.result.goalsAwayTeam) {
      return fixture.homeTeamName;
    } /*Else, if away team won*/else if (fixture.result.goalsHomeTeam < fixture.result.goalsAwayTeam) {
      return fixture.awayTeamName;
    } else {
      return 'Tie';
    }
  }
  getNumGoals(standing: LeagueObjectStanding) {
    let count = 0;
    this.getFilteredFixtures().forEach(fixture => {
      /*If played home team*/
      if (fixture.homeTeamName === standing.teamName) {
        count += fixture.result.goalsHomeTeam;
      } /*Else, if played away team*/ else {
        count += fixture.result.goalsAwayTeam;
      }
    });
    return count;
  }
  getNumWins(standing: LeagueObjectStanding) {
    let count = 0;
    this.getFilteredFixtures().forEach(fixture => {
      if (this.getFixtureWinner(fixture) === standing.teamName) {
        count ++;
      }
    });
    return count;
  }
  getWinner() {
    const aWins = this.getNumWins(this.teamAStanding);
    const bWins = this.getNumWins(this.teamBStanding);
    /*Determine winner by number of wins*/
    if (aWins > bWins) {
      return this.teamAStanding.teamName;
    } else if (aWins < bWins) {
      return this.teamBStanding.teamName;
    } else {
      /*If a tie, then determine winner by number of goals*/
      const aGoals = this.getNumGoals(this.teamAStanding);
      const bGoals = this.getNumGoals(this.teamBStanding);
      if (aGoals > bGoals) {
        return this.teamAStanding.teamName;
      } else if (aGoals < bGoals) {
        return this.teamBStanding.teamName;
      } else {
        return 'Tiebreaker';
      }
    }
  }
}
