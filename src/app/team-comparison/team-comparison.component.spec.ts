import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamComparisonComponent } from './team-comparison.component';

describe('TeamComparisonComponent', () => {
  let component: TeamComparisonComponent;
  let fixture: ComponentFixture<TeamComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
