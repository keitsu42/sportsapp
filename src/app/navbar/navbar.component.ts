import { Component, OnInit } from '@angular/core';
import {StandingService} from '../standing.service';
import {LeagueObjectStanding} from '../LeagueTableObjects/standing';
import {HelperService} from '../helper.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  /*Define variable to store standing array*/
  standings: LeagueObjectStanding[];
  /*Create the standing service to get standing array*/
  constructor(
    private route: ActivatedRoute,
    private standingService: StandingService,
  ) { }
  getStandings(): void {
    this.standingService.getStandings().subscribe(standingsObservable => this.standings
      = HelperService.sortStandings(standingsObservable.standing)); /*Get standings in alphabetical order*/
  }
  ngOnInit() {
    this.getStandings();
  }
  getTeamID(standing: LeagueObjectStanding) {
    return HelperService.getTeamID(standing);
  }
  getSelectedStanding() {
    return StandingService.getSelectedStanding();
  }
}
