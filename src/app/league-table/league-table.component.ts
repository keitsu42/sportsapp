import {Component, OnInit } from '@angular/core';
import {LeagueObjectStanding} from '../LeagueTableObjects/standing';
import {StandingService} from '../standing.service';
import {HelperService} from '../helper.service';


@Component({
  selector: 'app-league-table',
  templateUrl: './league-table.component.html',
  styleUrls: ['./league-table.component.css']
})

export class LeagueTableComponent implements OnInit {

  /*Define variable to store standing array*/
  standings: LeagueObjectStanding[];
  /*Create the standing service to get standing array*/
  constructor(private standingService: StandingService) { }
  getStandings(): void {
    this.standingService.getStandings()
      .subscribe(standingsObservable => this.standings = standingsObservable.standing);
  }
  ngOnInit() {
    StandingService.setSelectedStanding(null);
    window.scrollTo(0, 0);
    /*Call function on initialisation*/
    this.getStandings();
  }
  getTeamID(standing: LeagueObjectStanding) {
    return HelperService.getTeamID(standing);
  }
  setSelectedStanding(standing: LeagueObjectStanding) {
    StandingService.setSelectedStanding(standing);
  }
}
