import { Injectable } from '@angular/core';
import {LeagueObjectStanding} from './LeagueTableObjects/standing';
import {FixtureFixture} from './Fixture/fixture';

@Injectable()
export class HelperService {
  constructor() { }
  /*Get the teamID of the standing object*/
  static getTeamID(standing: LeagueObjectStanding) {
    return standing._links.team.href.substring(standing._links.team.href.lastIndexOf('/') + 1);
  }
  /*Sort standings by alphabetical order*/
  static sortStandings(standings: LeagueObjectStanding[]) {
    return standings.sort(HelperService.compareStandings);
  }
  /*Compare two standing objects*/
  static compareStandings(a: LeagueObjectStanding, b: LeagueObjectStanding) {
    if (a.teamName > b.teamName) {
      return 1;
    } else if (a.teamName < b.teamName) {
      return -1;
    }
    return 0;
  }
  /*Get the result for a fixture*/
  static getFixtureResult(fixture: FixtureFixture, isHomeTeam: boolean): string {
    /*Compare the goals between home and away team*/
    if (fixture.result.goalsHomeTeam > fixture.result.goalsAwayTeam) {
      return isHomeTeam ? 'Win' : 'Loss';
    } else if (fixture.result.goalsHomeTeam < fixture.result.goalsAwayTeam) {
      return isHomeTeam ? 'Loss' : 'Win';
    } else {
      return 'Draw';
    }
  }
}
