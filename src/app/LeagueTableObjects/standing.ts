import {LeagueObjectLinks} from './links';

export class LeagueObjectStanding {
  _links: LeagueObjectLinks;
  position: number;
  teamName: string;
  crestURI: string;
  playedGames: number;
  points: number;
  goals: number;
  goalsAgainst: number;
  goalDifference: number;
  wins: number;
  draws: number;
  losses: number;
}
